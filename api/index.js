import axios from 'axios'
import Cookie from "js-cookie";
import API_HOST from '~/static/const.js'


export default {
  async getNotifications(user_id) {
    let { data } = await axios.post(`${API_HOST}/notifications/index`,
      { user_id: user_id },
      { headers: {'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': Cookie.get('token_local')}}
    );
    return data;
  },
  async getRecords() {
    let { data } = await axios.get(`${API_HOST}/records/index`);
    return data.data;
  },
  async sendRecord(text, user_id) {
    let { data } = await axios.post(`${API_HOST}/records/store`,
      { text: text, user_id: user_id},
      { headers: {'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': Cookie.get('token_local')}}
    );
    return data.data;
  },
  async sendComment(record_id, text, user_id) {
    let { data } = await axios.post(`${API_HOST}/comments/store`,
      { record_id: record_id, text: text, user_id: user_id},
      { headers: {'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': Cookie.get('token_local')}}
    );
    return data.data;
  },
  async sendUpdate(type, id, text, user) {
    let { data } = await axios.post(`${API_HOST}/`+type+`/update`,
      { id: id, text: text, user: user },
      { headers: {'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': Cookie.get('token_local')}}
    );
    return data.data;
  },
  async sendDelete(type, id, user) {
    let { data } = await axios.post(`${API_HOST}/`+type+`/delete`,
      { id: id, user: user },
      { headers: {'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': Cookie.get('token_local')}}
    );
    return data.data;
  }/*,
  async loginUser(formdata) {

    let { user } = await axios.post(`http://dev-microblog-api/api/v1/login`,
      { formdata },
      { headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}}
    );
    user = user.data

    Cookie.set('token', user.meta.token)

    return user;
  }*/
}
